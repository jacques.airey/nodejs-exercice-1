
const express = require('express');
const app = express();

app.use(express.json());

app.get('/hello', function (req, res) {
  res.send('Hello World');
})

app.post('/chat', function (req, res) {
  if (req.body.msg == 'ville') {
  res.send('Nous sommes a Paris');
} else if (req.body.msg == 'météo') {
  res.send('Il fait beau');
} else {
  res.send(req.body.msg);
  res.send('Je ne comprends pas');
}
})

app.listen(process.env.PORT || 3000);
